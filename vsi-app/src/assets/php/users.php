<?php
define('METHOD_POST','POST',true);
define('METHOD_GET','GET',true);
define('IMAGES_DIR',__DIR__.'/../images');
//require 'head.php';
require 'lib/Medoo.php'; // https://medoo.in/ 
use Medoo\Medoo;

require 'config.php';//||die("Erreur config.php");
// pour des raisons de securité on met les infos authentification dans un fichier separé
//ce fichier ne doit PAS aller sur le depot git.
if(!$CFG){
  $msg=array('erreur'=>'Erreur importation config.php');
  die(json_encode($msg));
}
$database = new Medoo($CFG);


$method=$_SERVER['REQUEST_METHOD'];

if(strcasecmp($method,METHOD_POST)==0){
  //méthode post

  if($_REQUEST['enregistrer']){
    enregistrer();// enregistrement info individu
    //echo json_encode($_REQUEST);
  }
}
elseif(strcasecmp($method,METHOD_GET)==0){
  //méthode get

  if(isset($_REQUEST['id'])){
    $id=$_REQUEST['id'];
    lire(intval($id));

  }else{
    $msg=array('error'=>'Requette indeterminee');
    die(json_encode($msg));
  }
}


function enregistrer(){
  global $database;
  global $update;
  global $date;

  $colomn = array('username','firstname','lastname','password','role','email','status','dob','tel','dept');

  $record = array();
  
  $record["username"] = trim($_REQUEST['username']);
  $record["firstname"] = trim($_REQUEST['firstname']);
  $record["lastname"] = trim($_REQUEST['lastname']);
  $record["password"] = trim($_REQUEST['password']);
  $record["role"] = trim($_REQUEST['role']);
  $record["email"] = trim($_REQUEST['email']);
  $record["status"] = trim($_REQUEST['status']);
  $record["dob"] = trim($_REQUEST['dob']);
  $record["tel"] = trim($_REQUEST['tel']);
  $record["dept"] = trim($_REQUEST['dept']);

  $timestamp = time();
  $record["timemodified"] = $timestamp;
  $record["timecreated"] = $timestamp;
  $database -> insert("users",$record);

  if ($database -> id()) {
    $msg = array('message' => 'succes');
  } else {
    $msg = array('erreur' => 'echec');
  }

}

function modifier() {
  global $database;
  global $update;
  global $date;

  $compteur = 0;
  $colomn = array('username','firstname','lastname','password','role','email','status','dob','tel','dept');

  $record = array();

  $record["username"] = trim($_REQUEST['username']);
  $record["firstname"] = trim($_REQUEST['firstname']);
  $record["lastname"] = trim($_REQUEST['lastname']);
  $record["password"] = trim($_REQUEST['password']);
  $record["role"] = trim($_REQUEST['role']);
  $record["email"] = trim($_REQUEST['email']);
  $record["status"] = trim($_REQUEST['status']);
  $record["dob"] = trim($_REQUEST['dob']);
  $record["tel"] = trim($_REQUEST['tel']);
  $record["dept"] = trim($_REQUEST['dept']);

  $fields = '*';

  $where = array();

  $where['id'] = interval(trim($_REQUEST['id']));
  $data = $database -> select('users', "$fields", $where);

  if ($data[0]) {
    for ($i = 0 ; $i < sizeof($colomn) ; $i++) {
      if ($record[$column[$i]] == "" || $record[$column[$i]] == NULL) {
        unset($record[$column[$i]]);
        $compteur++;
      }
    }

    if ($record["password"] == "") {
      $record["password"] = $data["password"];
    }

    if ($compteur < sizeof($colomn)) {
      $timestamp = time();
      $record["timemodified"] = $timestamp;
      $database -> update("users",$record,["id" => $_REQUEST['id']]);
    }
  } else {
    echo "<script type='text/javascript'> alert('Id saisie n\'est pas dans la liste')</script>";
  }
}

function lire(int $id=0){
 global $database;
  $fields= [
     "id",
     "usename",
     "password",
     "firstname",
     "lastname",
     "role",
     "photo",
     "contenthash",
     "email",
     "status",
     "dob",
     "tel",
     "dept"
  ];
  $where=[];

if($id>0){
  $where['id[=]']=$id;//un individu specific
}
$records=$database->select("users",$fields,$where);
echo json_encode($records);
}

?>
