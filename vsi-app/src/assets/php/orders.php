<?php
define('METHOD_POST','POST',true);
define('METHOD_GET','GET',true);
define('IMAGES_DIR',__DIR__.'/../images');

require 'lib/Medoo.php'; // https://medoo.in/ 
use Medoo\Medoo;

require 'config.php';

if(!$CFG){
  $msg=array('erreur'=>'Erreur importation config.php');
  die(json_encode($msg));
}

$database = new Medoo($CFG);

$method=$_SERVER['REQUEST_METHOD'];

if(strcasecmp($method,METHOD_POST)==0){

  if($_REQUEST['enregistrer']){
    enregistrer();
  }
}

elseif (strcasecmp($method,METHOD_GET)==0){

  if(isset($_REQUEST['oid'])){
    $id=$_REQUEST['oid'];
    lire(intval($oid));

  } else {
    $msg=array('error'=>'Requette indeterminee');
    die(json_encode($msg));
  }
}


function enregistrer(){
  global $database;
  $record=array();

  $record["customer"]=$_REQUEST['customer'];
  $record["total"]=$_REQUEST['total'];
  $record["items"]=$_REQUEST['items'];

  $timestamp = time();
  $record["timecreated"] = $timestamp;

  $database -> insert("orders",$record);

  if ($database -> oid()) {
    $msg = array('message' => 'succes');
  } else {
    $msg = array('erreur' => 'echec');
  }
}

function lire(){
  global $database;
   $fields= [
      "oid",
      "customer",
      "total",
      "items",
   ];
   $where=[];
  $records=$database->select("users",$fields,$where);
  echo json_encode($records);
 }

?>