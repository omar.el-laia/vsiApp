<?php
define('METHOD_POST','POST',true);
define('METHOD_GET','GET',true);
define('IMAGES_DIR',__DIR__.'/../images');
//require 'head.php';
require 'lib/Medoo.php'; // https://medoo.in/ 
use Medoo\Medoo;

require 'config.php';

if(!$CFG){
  $msg=array('erreur'=>'Erreur importation config.php');
  die(json_encode($msg));
}
$database = new Medoo($CFG);


$method=$_SERVER['REQUEST_METHOD'];

if(strcasecmp($method,METHOD_POST)==0){
  //méthode post

  if($_REQUEST['enregistrer']){
    enregistrer();
  }
}
elseif (strcasecmp($method,METHOD_GET)==0){

  if(isset($_REQUEST['pki'])){
    $id=$_REQUEST['pki'];
    lire(intval($id));

  } else {
    $msg=array('error'=>'Requette indeterminee');
    die(json_encode($msg));
  }
}


function enregistrer(){
  global $database;
  global $update;
  global $date;

  $colomn = array('name','cat','description','photo','contenthash','video','prix','quantite');

  $record = array();
  
  $record["name"] = trim($_REQUEST['name']);
  $record["cat"] = trim($_REQUEST['cat']);
  $record["description"] = trim($_REQUEST['description']);
  $record["photo"] = trim($_REQUEST['photo']);
  $record["contenthash"] = trim($_REQUEST['contenthash']);
  $record["video"] = trim($_REQUEST['video']);
  $record["prix"] = trim($_REQUEST['prix']);
  $record["quantite"] = trim($_REQUEST['quantite']);

  $timestamp = time();
  $record["timemodified"] = $timestamp;
  $record["timecreated"] = $timestamp;
  $database -> insert("users",$record);

  if ($database -> pki()) {
    $msg = array('message' => 'succes');
  } else {
    $msg = array('erreur' => 'echec');
  }

}

function modifier() {
  global $database;
  global $update;
  global $date;

  $compteur = 0;
  $colomn = array('name','cat','description','photo','contenthash','video','prix','quantite');

  $record = array();

  $record["name"] = trim($_REQUEST['name']);
  $record["cat"] = trim($_REQUEST['cat']);
  $record["description"] = trim($_REQUEST['description']);
  $record["photo"] = trim($_REQUEST['photo']);
  $record["contenthash"] = trim($_REQUEST['contenthash']);
  $record["video"] = trim($_REQUEST['video']);
  $record["prix"] = trim($_REQUEST['prix']);
  $record["quantite"] = trim($_REQUEST['quantite']);
  $fields = '*';

  $where = array();

  $where['pki'] = interval(trim($_REQUEST['pki']));
  $data = $database -> select('products', "$fields", $where);

  if ($data[0]) {
    for ($i = 0 ; $i < sizeof($colomn) ; $i++) {
      if ($record[$column[$i]] == "" || $record[$column[$i]] == NULL) {
        unset($record[$column[$i]]);
        $compteur++;
      }
    }

    if ($record["password"] == "") {
      $record["password"] = $data["password"];
    }

    if ($compteur < sizeof($colomn)) {
      $timestamp = time();
      $record["timemodified"] = $timestamp;
      $database -> update("users",$record,["pki" => $_REQUEST['pki']]);
    }
  } else {
    echo "<script type='text/javascript'> alert('pki saisie n\'est pas dans la liste')</script>";
  }
}

function lire(int $pki=0){
 global $database;
  $fields= [
     "pki",
     "name",
     "cat",
     "description",
     "photo",
     "contenthash",
     "video",
     "prix",
     "quantite"
  ];
  $where=[];

if($pki>0){
  $where['pki[=]']=$pki;
}
$records=$database->select("products",$fields,$where);
echo json_encode($records);
}

?>