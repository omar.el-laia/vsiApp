<?php 

    namespace managerPage;
    require 'lib/Medoo';
    require 'config.php';
    use Medoo\Medoo;




    function lireCommandes() {
        global $database;
        $fields= [
            "oid",
            "customer",
            "total",
            "items",
        ];
        $where=[];
        $records=$database->select("orders",$fields,$where);
        echo json_encode($records);
    }

    function lireIdUsers() {
        global $database;

        $where=[];
        $records=$database->select("users","id",$where);

        echo json_encode($records);
    }



?>

<div class="formAjout">
    <label for="id">Id :</label>
    <input type="text" name="id" id="id">

    <label for="username">Nom d'utilisateur : </label>
    <input type="password" name="password" id="password">

    <label for="firstname">Prénom : </label>
    <input type="text" name="firstname" id="firstname">

    <label for="lastname">Nom : </label>
    <input type="text" name="lastname" id="lastname">
    
    <label for="role">Role : </label>
    <select name="role" id="role">
        <option value="user">Utilisateur</option>
        <option value="admin">Admin</option>
    </select>

    <label for="email">E-mail : </label>
    <input type="email" name="email" id="email">

    <label for="status">Status : </label>
    <select name="status" id="status">
        <option value="employe">Employe</option>
        <option value="client">Client</option>
    </select>

    <label for="dob">Date de naissance : </label>
    <input type="date" name="dop" id="dob">

    <label for="tel">Téléphone : </label>
    <input type="text" name="tel" id="tel">

    <label for="dept">Departement : </label>
    <select name="dept" id="dept">
        <option value="dev">Développement</option>
        <option value="fc">Formation continue</option>
        <option value="sc">Service conseil</option>
        <option value="infra">Infrastructure</option>
    </select>

    <button type="submit" name="moficationForm" value="moficationForm"></button>

</div>


<div class="formModification">
    <label for="id">Id :</label>
    <select name="id" id="id">
        <?php
            $tableId = lireIdUsers();
            foreach($tableId as $id) 
                echo "<option value '{$id}'> {$id}</option>";
        ?>
    </select>
    <input type="text" name="id" id="id">

    <label for="username">Nom d'utilisateur : </label>
    <input type="password" name="password" id="password">

    <label for="firstname">Prénom : </label>
    <input type="text" name="firstname" id="firstname">

    <label for="lastname">Nom : </label>
    <input type="text" name="lastname" id="lastname">
    
    <label for="role">Role : </label>
    <select name="role" id="role">
        <option value="user">Utilisateur</option>
        <option value="admin">Admin</option>
    </select>

    

    <label for="email">E-mail : </label>
    <input type="email" name="email" id="email">

    <label for="status">Status : </label>
    <select name="status" id="status">
        <option value="employe">Employe</option>
        <option value="client">Client</option>
    </select>

    <label for="dob">Date de naissance : </label>
    <input type="date" name="dop" id="dob">

    <label for="tel">Téléphone : </label>
    <input type="text" name="tel" id="tel">

    <label for="dept">Departement : </label>
    <select name="dept" id="dept">
        <option value="dev">Développement</option>
        <option value="fc">Formation continue</option>
        <option value="sc">Service conseil</option>
        <option value="infra">Infrastructure</option>
    </select>

    <button type="submit" name="moficationForm" value="moficationForm"></button>
</div>


<div class="listeCommandes">
    <?php
        $tableCommandes = lireCommandes();
        echo "($tableCommande)";
    ?>
</div>