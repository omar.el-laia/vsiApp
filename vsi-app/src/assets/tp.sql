-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Jul 31, 2022 at 04:10 PM
-- Server version: 8.0.29
-- PHP Version: 8.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tp`
--

-- --------------------------------------------------------

--
-- Table structure for table `tp_orders`
--

CREATE TABLE IF NOT EXISTS `tp_orders` (
  `oid` int NOT NULL AUTO_INCREMENT COMMENT 'identifiant de commande',
  `customer` char(100) NOT NULL COMMENT 'Références à username de la table tp_users',
  `total` int NOT NULL,
  `timecreated` int DEFAULT NULL,
  `items` text NOT NULL COMMENT 'références aux produits commandés',
  PRIMARY KEY (`oid`),
  KEY `customer` (`customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

----------------------------------------------------------

--
-- Table structure for table `tp_products`
--

CREATE TABLE IF NOT EXISTS `tp_products` (
  `pki` int NOT NULL AUTO_INCREMENT,
  `name` char(100) NOT NULL,
  `cat` enum('dev','infra','sc','fc') NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `photo` char(128) DEFAULT NULL COMMENT 'nom deonné par l''utilisateur au fichier photo du produit',
  `contenthash` char(40) DEFAULT NULL COMMENT 'sha1 de la photo du produit. C''est nom  sous lequel sera enregistré la photo',
  `video` char(255) DEFAULT NULL,
  `prix` int NOT NULL,
  `quantite` int NOT NULL DEFAULT '0',
  `timecreated` int NOT NULL,
  `timemodified` int NOT NULL,
  PRIMARY KEY (`pki`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tp_users`
--

CREATE TABLE IF NOT EXISTS `tp_users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` char(100) NOT NULL COMMENT 'Nom d''utilisateur',
  `password` char(255) NOT NULL,
  `firstname` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `lastname` char(100) NOT NULL,
  `role` enum('admin','user') NOT NULL,
  `photo` char(128) DEFAULT NULL COMMENT 'Nom que l''utilisateur avait donné au fichier photo',
  `contenthash` char(40) DEFAULT NULL COMMENT 'sha1 du contenu du fichier photo. C''est le nom que portera le fichier enregistré sur le serveur.',
  `email` char(100) NOT NULL,
  `status` enum('customer','employee') NOT NULL COMMENT 'Status de la personne.',
  `dob` int DEFAULT NULL COMMENT 'Date de naissance',
  `tel` char(20) DEFAULT NULL COMMENT 'Téléphone',
  `dept` enum('dev','infra','sc','fc') DEFAULT NULL COMMENT 'Département',
  `timecreated` int DEFAULT NULL COMMENT 'timestamp de la date création du compte',
  `timemodified` int DEFAULT NULL COMMENT 'timestamp de la date modification du compte',
  `lastlogin` int DEFAULT NULL COMMENT 'timestamp de la date de dernière connexion de l''utilisateur',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tp_users`
--

INSERT INTO `tp_users` (`id`, `username`, `password`, `firstname`, `lastname`, `role`, `photo`, `contenthash`, `email`, `status`, `dob`, `tel`, `dept`, `timecreated`, `timemodified`, `lastlogin`) VALUES
(1, 'admin', 'c620ca92831a71e13a03d7d293efaa33', 'Administrator', 'System', 'admin', NULL, NULL, 'admin@inf3190.test.web', 'employee', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'client1', '2f4fa55339d7cb2bebb325742f3d54c9', 'Client', 'Usager', 'user', NULL, NULL, 'client1@inf3190.test.web', 'customer', NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tp_orders`
--
ALTER TABLE `tp_orders`
  ADD CONSTRAINT `tp_orders_ibfk_1` FOREIGN KEY (`customer`) REFERENCES `tp_users` (`username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
