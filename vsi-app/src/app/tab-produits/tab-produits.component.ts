import { Component, Input, OnInit } from '@angular/core';
import { BdService } from '../bd.service';

@Component({
  selector: 'app-tab-produits',
  templateUrl: './tab-produits.component.html',
  styleUrls: ['./tab-produits.component.css']
})


export class TabProduitsComponent implements OnInit {

  @Input('categorie') categorie!:string;
  @Input('titre') titre!:string;

  tableau:any;

  constructor(protected bdService: BdService) { }

  ngOnInit(): void {
    this.tableau = {
      categorie:this.categorie,
      titre:this.titre
    }
  }

  getProduits():any[] {
    return this.bdService.getProdsCategorie(this.categorie);
  }

}
