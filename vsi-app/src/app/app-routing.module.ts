import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { AjouterEmployeComponent } from './ajouter-employe/ajouter-employe.component';
import { CarouselComponent } from './carousel/carousel.component';
import { ContactComponent } from './contact/contact.component';
import { DevComponent } from './dev/dev.component';
import { EmployesComponent } from './employes/employes.component';
import { FcComponent } from './fc/fc.component';
import { InfraComponent } from './infra/infra.component';
import { LoginComponent } from './login/login.component';
import { PanierComponent } from './panier/panier.component';
import { ScComponent } from './sc/sc.component';


const routes: Routes = [
  {path:"",component:CarouselComponent},
  {path:"dev",component:DevComponent},
  {path:"fc", component:FcComponent},
  {path:"infra", component:InfraComponent},
  {path:"sc", component:ScComponent},
  {path:"admin", component:AdminComponent},
  {path:"contact", component:ContactComponent},
  {path:"employes", component:EmployesComponent},
  {path:"login", component:LoginComponent},
  {path:"panier", component:PanierComponent},
  {path:"ajouterEmploye", component:AjouterEmployeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
