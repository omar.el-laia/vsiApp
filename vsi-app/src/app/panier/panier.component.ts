import { Component, OnInit } from '@angular/core';
import { BdService } from '../bd.service';
import { LoginlogoutService } from '../loginlogout.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.css']
})
export class PanierComponent implements OnInit {

  constructor(private router:Router, 
    protected bdService:BdService, 
    protected loginlogoutService:LoginlogoutService) { }

  ngOnInit(): void {
  }

  calculerTotal():number {
    let total:number = 0;
    let panier:any [] = this.bdService.getPanier();
    for(let elt of panier) {
      total = total + elt.prix;
    }

    return total; 
  }

  viderPanier() {
    this.bdService.lstPanier = [];
  }
  
  
  commander() {
    let commande:any = new FormData();
    const enregistrer = 'enregistrer';
    let panier:any[] = this.bdService.getPanier();

    if (panier.length > 0) {
      commande.append('customer',this.loginlogoutService.userConnecte);
      commande.append('total',this.calculerTotal());
      commande.append('items',panier);
      commande.append(enregistrer,enregistrer);

      this.bdService.postData('/php/orders.php',commande);
      this.viderPanier();

      alert('La commande a été effectuée');

      this.router.navigateByUrl('');
    }
  }

}
