import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AdminComponent } from './admin/admin.component';
import { CarouselComponent } from './carousel/carousel.component';
import { ContactComponent } from './contact/contact.component';
import { DevComponent } from './dev/dev.component';
import { EmployesComponent } from './employes/employes.component';
import { FcComponent } from './fc/fc.component';
import { InfraComponent } from './infra/infra.component';
import { ScComponent } from './sc/sc.component';
import { LoginComponent } from './login/login.component';
import { PanierComponent } from './panier/panier.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HttpClientModule } from '@angular/common/http';
import { TabProduitsComponent } from './tab-produits/tab-produits.component';
import { AjouterEmployeComponent } from './ajouter-employe/ajouter-employe.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    CarouselComponent,
    ContactComponent,
    DevComponent,
    EmployesComponent,
    FcComponent,
    InfraComponent,
    ScComponent,
    LoginComponent,
    PanierComponent,
    HeaderComponent,
    FooterComponent,
    TabProduitsComponent,
    AjouterEmployeComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
