import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BdService } from '../bd.service';

@Component({
  selector: 'app-ajouter-employe',
  templateUrl: './ajouter-employe.component.html',
  styleUrls: ['./ajouter-employe.component.css']
})
export class AjouterEmployeComponent implements OnInit {
      savestatus:string = "";
      username = new FormControl('', Validators.required);
      pwd  = new FormControl('', Validators.required);
      lname = new FormControl('', Validators.required);
      fname = new FormControl('',Validators.required);
      role = new FormControl('');
      photo = new FormControl('');
      email = new FormControl('',Validators.email);
      status = new FormControl('',Validators.required);
      dob = new FormControl('');
      tel = new FormControl('');
      debt = new FormControl('');
  

  constructor(private bdService: BdService) { }

  ngOnInit(): void {
  }

  saveUser() {
    let utilisateur:any = new FormData();

    utilisateur.append("username",this.username.value);
    utilisateur.append("pw",this.pwd.value);
    utilisateur.append("lname",this.lname.value);
    utilisateur.append("fname",this.fname.value);
    utilisateur.append("role",this.role.value);
    utilisateur.append("photo",this.photo.value);
    utilisateur.append("email",this.email.value);
    utilisateur.append("status",this.status.value);
    utilisateur.append("dob",this.dob.value);
    utilisateur.append("tel",this.tel.value);
    utilisateur.append("debt",this.debt.value);
    
    let obs= this.bdService.postData('/php/users.php',utilisateur);

    alert('l\'utilisateur à été ajouté');
  }



}
