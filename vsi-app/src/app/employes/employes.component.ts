import { Component, OnInit } from '@angular/core';
import { BdService } from '../bd.service';

@Component({
  selector: 'app-employes',
  templateUrl: './employes.component.html',
  styleUrls: ['./employes.component.css']
})
export class EmployesComponent implements OnInit {

  constructor(private bdService: BdService) { }

  ngOnInit(): void {
  }

  getUsagers():any[] {
    this.bdService.getUsers();
    return this.bdService.listeUsers;
  }


}
