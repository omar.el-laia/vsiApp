import { getLocaleDateFormat } from '@angular/common';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class BdService {
  
  private baseUrl:string = "http://localhost:4200/vsi-app/src/assets/";// TODO Corriger 
  
  lstPanier:string[] = [];
  listeProduits:any[] = [];
  listeUsers:any[] =[];

  qerror:any= {};
  qcomplete: number = 0;

  constructor(private httpClient: HttpClient) {}



getData(url:string, para:any={}) {
  let getUrl: string = this.baseUrl + url;
  return this.httpClient.get<any>(getUrl,{params:para});
}

getUsers(id:number=0) {
  const qurl: string = '/php/users.php';
  const qparams = { 'id': id };
  this.getData(qurl,qparams).subscribe({
    next: (data: any) => { console.log(data); this.listeUsers = data },
    error: (err: any) => { console.info(err); this.qerror = err },
    complete: () => {
      console.info("completed"); this.qcomplete = 1;
      
    }

  });
}

postData(url:string, data:FormData){
  let postUrl:string = this.baseUrl + url;
  this.httpClient.post<any>(postUrl, data).subscribe({
    next: (res) => console.log(res),
    error:(err) => console.log(err),
    complete: () => console.info('complete')
  });
}

updateProduit(data:FormData) {
  this.postData('/php/products.php',data);
}

getProduits(pki = 0){
  const qurl: string = '/php/products.php';
  const qparams = { 'pki': pki };
  this.getData(qurl,qparams).subscribe({
    next: (data: any) => { console.log(data); this.listeUsers = data },
    error: (err: any) => { console.info(err); this.qerror = err },
    complete: () => {
      console.info("completed"); this.qcomplete = 1;
      
    }
  });
}

getProdsCategorie(categorie:string):any[] {
  this.getProduits();
  let listeCat:any [] = [];
  let i:number = 0;

  for(let prod of this.listeProduits) {
    if(categorie.localeCompare(prod.cat) == 0) {
      listeCat[i] = prod;
      i = i+1;
    }
  }

  return listeCat;
}


getPanier():any{
  let panier:any[] = [];
  for(let srv of this.listeProduits) {
    if (this.estElementDeListe(srv.pki,this.lstPanier)) {
      panier.push(srv);
    }
  }
  return panier;
}

togglePanier(pki:string){
  if (this.lstPanier.length > 0) {
    if(this.estElementDeListe(pki,this.lstPanier)) {
      this.lstPanier.forEach((element,index) => {
        if(element == pki) {
          delete this.lstPanier[index]
        }
      });
    } else {
      this.lstPanier.push(pki);
    }
  } else {
    this.lstPanier.push(pki);
  }
    
} 

estElementDeListe(element:any,liste:any[]) {
  let estElement:boolean = false; 
  if (liste.length > 0) {
    for (let elt of liste) {
        if (elt == element) {
          estElement = true;
        }
      }
  }
  
  return estElement;
}

}

