import { Component, OnInit } from '@angular/core';
import { BdService } from '../bd.service';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {


  
  constructor(private bdServices: BdService, private lgIn: LoginComponent) { }
  
  
  ngOnInit(): void {
  }
  getClients():any[] {
    let listeClients:any[] = [];
    this.bdServices.getUsers();
    for(let user of this.bdServices.listeUsers) {
      if(user.role == "customer") {
        listeClients.push(user);
      }
    }
  
    return listeClients;
  }
}
