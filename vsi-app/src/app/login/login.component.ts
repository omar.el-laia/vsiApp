import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BdService } from '../bd.service';
import { LoginlogoutService } from '../loginlogout.service';

var estUserAdmin:boolean = false; 

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = new FormControl('',Validators.required);
  pwd = new FormControl('',Validators.required);
  msgErr:string = "";

  entreeValide:boolean = false;
  statusConnexion:string = "";


  constructor(private router:Router,private bdService: BdService, private loginlogoutSrevice: LoginlogoutService) { }

  ngOnInit(): void {
  }

  seConnecter() {
    return this.msgErr = this.loginlogoutSrevice.login(this.username.value!,this.pwd.value!);
  }
  
}
