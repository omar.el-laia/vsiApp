import { Injectable } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { Md5 } from 'ts-md5';
import { BdService } from './bd.service';

@Injectable({
  providedIn: 'root'
})
export class LoginlogoutService {

  userConnecte:string = 'admin';
  roleUserConnecte:string = 'admin';
  msgErr:string = '';

  constructor(private bdService:BdService, private route:ActivatedRoute,private router : Router) { }


  login(username:string, pw:string) {
    if(this.userConnecte == '') {
      const md5 = new Md5;
      const pwCrypte:any = md5.appendStr(pw).end();
      let userValide:boolean = false;
      let i = 0;
      while (i < this.bdService.listeUsers.length || userValide) {
        if (this.bdService.listeUsers[i].username.localeCompare(username.trim())== 0 &&this.bdService.listeUsers[i].pw.localeCompare(pwCrypte)== 0 ) {
          this.userConnecte = this.bdService.listeUsers[i].username;
          this.roleUserConnecte = this.bdService.listeUsers[i].role;
          userValide = true;
        }
        i++
      }

      if (userValide) {
        if (this.roleUserConnecte == 'admin') {
          this.router.navigateByUrl('admin');
        } else if (this.roleUserConnecte == 'user') {
          this.router.navigateByUrl('panier');
        }
        this.msgErr = '';
      } else {
        this.msgErr = 'Nom d\'utilisateur ou mot de passe invalide'; 
      }
      
    } else {
      this.logout();
    }
    return this.msgErr;
  }

  logout() {
    this.roleUserConnecte = '';
    this.userConnecte = '';
    this.router.navigateByUrl('');

  }


}
