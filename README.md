# Projet de session d'INF3190

## _Table des matières_

* 1.[Résumé du projet](#rsum-du-projet)
* 2.[Statut du projet](#statut-du-projet)
* 3.[Exigences](#les-exigences-concernant-l'environnement-de-developpement)
* 5.[Technologies](#technologies)
* 6.[Déploiement](#déploiement)
* 7.[Auteurs](#auteurs-de-ce-projet-de-session)


## _Résumé du projet_
Ce projet a été développé dans le cadre du cours INF3190 de l'UQAM.

Vente des Services Informatiques (vsi-app) est une application Angular qui fonctionne comme un site monopage. Elle permet d'effectuer des commandes de produits/services informatiques dans les catégories développement, infrastructure, formations continues et services conseils.

L'application permet aussi au personnes autorisés d'ajouter et modifier les utilisateurs dans une base de données SQL.


## _Statut du projet_
**Le développement de l'application s'est déroulé en 3 itérations :**  
- _Première itération :_ La construction du site et d'une base pour les différentes pages et fonctionnalités qui seront proposés.

- _Deuxième itération :_ La création d'une application Angular en utilisant aussi les technologies JSON.

- _Troisième itération :_ La suite de l'implémentation des fonctionnalités et remplacement des données formatés sous forme de fichiers JSON par une base de données dont les requêtes SQL sont gérées avec du PHP.

## _Les exigences concernant l'environnement de développement du site_
L'application web fonctionne comme un site monopage et contient de multiples composants et services qui sont pertinnent à la navigation entre les differents pages et de l'utilisation des fonctionnalités proposés.

## _Technologies :_
Le projet a été crée à partir de la technologie **Angular.js et Node.js** en plus du langage **PHP** pour le back-end et **SQL** pour la gestions des requêtes vers la base de données 

## _Déploiement :_

- Cloner le dépot du projet :
~~~csh 
git clone https://gitlab.info.uqam.ca/el-laia.omar/inf3190-e22-tp.git 
~~~
- Se déplacer dans le dossier de l'application :
~~~csh 
cd inf3190-e22-tp/vsi-app
~~~
- Installer les dependances : 
~~~csh 
npm install
~~~
- Lancer l'application sur le moteur de recherche :
~~~csh
ng serve -o
~~~
